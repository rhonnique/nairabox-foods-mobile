import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View, AsyncStorage } from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Colors from './constants/Colors';

import BottomTabNavigator from './navigation/BottomTabNavigator';
import useLinking from './navigation/useLinking';
import { API_URL, REQUEST_TIMEOUT } from 'react-native-dotenv'
import Details from './screens/Details';
import RestaurantDetailsScreen from './screens/restaurant/RestaurantDetailsScreen';

const Stack = createStackNavigator();

export const AppContext = React.createContext();

import axios from "axios";
axios.defaults.timeout = REQUEST_TIMEOUT;

export const Axios = axios.create({
  baseURL: API_URL,
  timeout: 1000000,
  headers: { 'Accept': 'application/json' },
});

/* Axios.interceptors.request.use(config => {
  // perform a task before the request is sent
  console.log('Request was sent');

  return config;
}, error => {
  // handle the error
  console.log('error error error...');
  return Promise.reject(error);
}); */

/* Axios.interceptors.response.use((response) => {
  // do something with the response data
  console.log('Response was received');

  return response;
}, error => {
  // handle the response error
  console.log(error, 'error error error...');
  return Promise.reject(error);
});
 */

const StatusBarSet = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

export default function App(props) {
  const [title] = React.useState('Nairabox foods');
  const [currentScreen, currentScreenSet] = React.useState(null);
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  const [savedState, setSavedState] = React.useState(null);
  const [savedCity, setSavedCity] = React.useState(null);

  const groupBy = (list, keyGetter) => {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }

  const makeId = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  const setCurrentScreen = async (data) => {
    currentScreenSet(data);
  };

  const setStorageObj = async (name, value) => {
    await AsyncStorage.setItem(name, JSON.stringify(value));
  };

  const setStateCity = async (state, city) => {
    //console.log(state, city, 'set both...')
    await setStorageObj('setState', state); setSavedState(state);
    await setStorageObj('setCity', city); setSavedCity(city);
  };

  const getSingleArray = (data, pos) => {
    return data && Array.isArray(data) && data.length > 0 ? data[pos] : null;
  }

  const isData = (data) => {
    return data && Array.isArray(data) && data.length > 0;
  }

  const isDataEmpty = (data) => {
    return data && Array.isArray(data) && data.length === 0;
  }

  const objectExist = (data, value) => {
    return data && data instanceof Object && data[value];
  }

  const getRouteParam = (route, value) => {
    return objectExist(route.params, value) ? route.params[value] : null;
  }


  const getStates = async () => {
    const response = await Axios.get(`/site/city`);
    return response;
  }

  const getCities = async (id) => {
    const response = await Axios.get(`/site/locationByCity/${id}`);
    return response;
  }

  const getFeaturedRestaurants = async (id) => {
    const response = await axios.get(`/site/restaurantByFeatured/${id}`);
    return response;
  }

  const getRestaurants = async (id) => {
    const response = await Axios.get(`/site/restaurantBylocation/${id}`);
    return response;
  }

  /* const getRestaurantDetails = async (id) => {
    const response = await axios.get(`/site/restaurant/${id}`);
    return response;
  } */

  const getRestaurantDetails = async (id) => {
    const response = await Axios.get('/meals.json');
    return response;
  }

  const restaurantMealsCatGroup = async (id) => {
    const response = await Axios.get(`/site/mealCatByRestaurant/${id}`);
    return response;
  }

  const restaurantMealsCat = async (restaurantId, catId) => {
    const response = await Axios.get(`/site/mealByRestaurantCat/${restaurantId}/${catId}`);
    return response;
  }

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    /* axios.request().catch(function (error) {
      if (!error.status) {
        console.log('net error....')
      }
    }); */




    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        });

        await Asset.loadAsync([
          require("./assets/images/splash.png"),
          require("./assets/images/home-bg.png"),
          require("./assets/images/nbxlogo-animated.gif"),
          require("./assets/images/an.gif"),
        ]);

        setSavedState(JSON.parse(await AsyncStorage.getItem("setState")) || null);
        setSavedCity(JSON.parse(await AsyncStorage.getItem("setCity")) || null);
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return null;
  } else {
    return (
      <AppContext.Provider value={{
        /* states */
        title,
        savedState,
        savedCity,

        groupBy,
        isData,
        objectExist,
        isDataEmpty,
        setStorageObj,
        setStateCity,
        getRouteParam,
        getSingleArray,
        setCurrentScreen,
        currentScreen,
        makeId,

        getCities,
        getStates,
        getRestaurants,
        getFeaturedRestaurants,
        getRestaurantDetails,
        restaurantMealsCatGroup,
        restaurantMealsCat
      }}>
        <View style={styles.container}>
          {/* <StatusBarSet backgroundColor={Colors.bgDefault} barStyle="light-content" /> */}
          <StatusBar
            translucent
            barStyle="light-content"
          /* backgroundColor="rgba(0, 0, 0, 0.251)" */
          />
          <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
            <Stack.Navigator>
              <Stack.Screen name="Root" component={BottomTabNavigator} />
              <Stack.Screen name="RestaurantDetailsScreen"
                component={RestaurantDetailsScreen}
                options={{
                  headerShown: false
                }}
              />
              <Stack.Screen name="Details"
                options={{ title: 'Olaiya Foods', headerBackTitle: 'Backs' }}
                component={Details} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </AppContext.Provider>
    );
  }
}

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  }
});
