import React from "react";
import { StyleSheet } from "react-native";
import Colors from '../constants/Colors';
import Layout from '../constants/Layout';

export default StyleSheet.create({

  /* Paddings */
  pt5: {
    paddingTop: 5
  },
  pt10: {
    paddingTop: 10
  },
  pt15: {
    paddingTop: 15
  },
  p15: {
    padding: 15
  },
  py10: {
    paddingVertical: 10
  },
  py15: {
    paddingVertical: 15
  },
  py20: {
    paddingVertical: 20
  },
  py25: {
    paddingVertical: 25
  },
  py30: {
    paddingVertical: 30
  },

  px5: {
    paddingHorizontal: 5
  },
  px10: {
    paddingHorizontal: 10
  },
  px15: {
    paddingHorizontal: 15
  },
  mb1: {
    marginBottom: 1
  },
  mb2: {
    marginBottom: 2
  },
  mb5: {
    marginBottom: 5
  },
  mb20: {
    marginBottom: 20
  },
  mb25: {
    marginBottom: 25
  },
  mb30: {
    marginBottom: 30
  },
  mb35: {
    marginBottom: 35
  },
  mx5: {
    marginBottom: 5
  },

  mx10: {
    marginHorizontal: 10
  },
  mx15: {
    marginHorizontal: 15
  },

  /* Colors */
  colorMute: {
    color: Colors.textMute
  },
  colorLightMd: {
    color: '#999'
  },
  colorWhite: {
    color: '#fff'
  },

  /* Fonts */
  font10: {
    fontSize: 10
  },
  font11: {
    fontSize: 11
  },
  font12: {
    fontSize: 12
  },
  font13: {
    fontSize: 13
  },
  font14: {
    fontSize: 14
  },
  font15: {
    fontSize: 15
  },
  font16: {
    fontSize: 16
  },
  font17: {
    fontSize: 17
  },
  font18: {
    fontSize: 18
  },
  font19: {
    fontSize: 19
  },
  font20: {
    fontSize: 20
  },
  fontWeightBold: {
    fontWeight: 'bold'
  },
  fontWeight600: {
    fontWeight: '600'
  },
  fontWeightNormal: {
    fontWeight: 'normal'
  },
  fontWeightLight: {
    fontWeight: '200'
  },

  h1: {
    color: Colors.textDark,
    fontSize: 20,
    marginBottom: 10,
    fontWeight: '600'
  },

  h2: {
    color: Colors.textDark,
    fontSize: 19,
    marginBottom: 10,
    fontWeight: '600'
  },

  h3: {
    color: Colors.textDark,
    fontSize: 18,
    marginBottom: 10,
    fontWeight: '600'
  },

  h4: {
    color: Colors.textDark,
    fontSize: 17,
    marginBottom: 10,
    fontWeight: '600'
  },

  h5: {
    color: Colors.textDark,
    fontSize: 16,
    marginBottom: 10,
    fontWeight: '600'
  },

  h6: {
    color: Colors.textTitle,
    fontSize: 15,
    marginBottom: 10,
    fontWeight: '600'
  },

  /* *** Container *** */

  container: {
    flex: 1,
    backgroundColor: Colors.colorWhite,
    position: 'relative'
  },
  containerFlexCC: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  cp: {
    padding: Layout.windowPadding
  },
  ct: {
    paddingTop: Layout.windowPadding
  },
  cb: {
    paddingBottom: Layout.windowPadding
  },
  cpy: {
    paddingVertical: Layout.windowPadding
  },
  cpx: {
    paddingHorizontal: Layout.windowPadding
  },
  cpl: {
    paddingLeft: Layout.windowPadding
  },
  cpr: {
    paddingRight: Layout.windowPadding
  },

  flexSpace: {
    flexDirection: "row",
    justifyContent: "space-between",
  },

  shadowContainer: {
    overflow: 'hidden',
    paddingBottom: 5
  },

  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },


  containerForm: {
    paddingBottom: 15,
    width: '100%'
  },
  formSelect: {
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 4,
    borderColor: Colors.textMute,
    borderWidth: 2,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 12,
    paddingVertical: 13
  },
  formSelectPlaceHolder: {
    color: Colors.textMute,
  },
  formSelectText: {
    fontWeight: '500',
    fontSize: 15,
    marginRight: 5
  },

  /* Restaurants */
  featuredContainer: {
    borderBottomColor: Colors.textLight,
    borderBottomWidth: 1,
    marginBottom: 15
  },
  featuredItemContainer: {
    marginHorizontal: 5,
  },
  featuredItemImage: {
    overflow: 'hidden',
    width: 100,
    height: 100,
    backgroundColor: Colors.textLight,
    borderRadius: 50,
    marginBottom: 10
  },
  featuredItemTitle: {
    width: 100,
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '600'
  },


  itemContainer: {
    marginBottom: 25,
  },
  ItemImage: {
    overflow: 'hidden',
    height: 200,
    backgroundColor: Colors.textLight,
    marginBottom: 10,
    position: 'relative'
  },
  ItemImageContentBottom: {
    position: 'absolute',
    bottom: 0,
    padding: 10
  },

  ItemContents: {
    paddingHorizontal: 10,
  },
  itemDeliveryTime: {
    paddingVertical: 5,
    paddingHorizontal: 8,
    backgroundColor: Colors.textWhite,
    color: Colors.bgDefault,
    borderRadius: 5,
    fontWeight: 'bold'
  },
  itemCusines: {
    color: '#949494',
    marginBottom: 3
  },
  itemData: {
    color: '#808080',
    fontWeight: '600'
  },

  itemTabContainer: {
    height: Layout.window.height - Layout.statusAppBarHeight,
    backgroundColor: '#fff'
  },

  /* **** SEARCH BOX **** */
  searchContainer: {
    height: 40,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ededed'
  },
  searchInput: {
    height: 40,
    borderWidth: 0,
    width: Layout.window.width - 40,
    paddingHorizontal: 10
  },
  searchButton: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignSelf: 'center'
  },

  /* **** Restaurant Details **** */
  listItemContainer: {
    borderBottomColor: Colors.borderColor,
    borderBottomWidth: 1,
    paddingVertical: 18
  },
  listItem: {
    width: Layout.window.width - 56
  },
  listItemBadge: {
    backgroundColor: Colors.bgDefault,
    height: 25,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  listItemBadgeText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 12
  },
  listItemTitle: {
    fontSize: 15,
    color: '#757F8C',
    marginBottom: 2,
    fontWeight: '600'
  },
  listItemPrice: {
    fontSize: 14,
    color: '#3B414B',
    fontWeight: 'bold'
  },

  rdSummaryPlaceholder: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    zIndex: 1,
  },
  rdSummaryContainer: {
    height: 50,
    backgroundColor: Colors.bgDefault,
    marginHorizontal: 15,
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  rdSummaryText: {
    color: '#fff',
    alignSelf: 'center',
    fontWeight: '500'
  },


  /* *** Preloader *** */
  plTab: { 
    flexDirection: "row", 
    width: '100%', 
    height: 48, 
    backgroundColor: 
    Colors.textLight, 
    paddingVertical: 12 
  },
  plTabInset: { 
    width: '28.3%', 
    height: 24, 
    backgroundColor: '#fff', 
    marginHorizontal: 8
  },
  plList: { 
    padding: 15, 
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Colors.borderColor, 
    flexDirection: "row"
  },

});