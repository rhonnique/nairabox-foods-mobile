import * as React from 'react';
import {
  StyleSheet, Text, View, Button, TouchableWithoutFeedback,
  Image, TouchableOpacity, ImageBackground
} from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import Colors from '../constants/Colors';
import _sty from "../assets/Styles";
import { Ionicons, Entypo, SimpleLineIcons } from '@expo/vector-icons';
import Layout from '../constants/Layout';
import { MAX_QTY } from 'react-native-dotenv';

const GIF_BG = '../assets/images/an.gif';

export function CustomHeader(props) {
  return (<View style={styles.header}>
    <Text style={styles.headerText}>{props.title}</Text>
  </View>);
}

export function Loader(props) {
  return (<View style={_sty.containerFlexCC}>
    <Image style={{ width: 70, height: 55, alignSelf: 'center' }}
      source={require('../assets/images/nbxlogo-animated.gif')} />
  </View>);
}

export function ErrorDisplay({ title, content, onPress }) {
  return (<View style={_sty.containerFlexCC}>
    <Entypo name='emoji-sad' size={50} color={Colors.textMute} style={_sty.mb5} />
    <Text style={[_sty.h6, _sty.mb2]}>{title ? title : 'Opps!!'}</Text>
    <Text style={[_sty.colorLightMd, _sty.mb5]}>
      {content ? content : 'Seem requested data can not be loaded.'}
    </Text>
    <TouchableOpacity onPress={onPress}>
      <Text style={[_sty.font12, _sty.fontWeight600]}>Click here to retry</Text>
    </TouchableOpacity>
  </View>);
}

export function EmptyDisplay({ title, content }) {
  return (<View style={_sty.containerFlexCC}>
    <SimpleLineIcons name='folder' size={50} color={Colors.textMute} style={_sty.mb5} />
    <Text style={[_sty.h6, _sty.mb2]}>{title ? title : 'Opps!!'}</Text>
    <Text style={[_sty.colorLightMd, _sty.mb5]}>
      {content ? content : 'Seem no result found.'}
    </Text>
  </View>);
}

export function defaultHeaderConfig() {
  return ({
    headerTintColor: Colors.textWhite,
    headerTitleAlign: 'center',
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: Colors.bgDefault,
      borderBottomWidth: 0
    },
    headerTitleStyle: {
      fontSize: 16
    }
  });
}

export function ListView({ icon, label, onPress, isLastOption }) {
  return (
    <RectButton style={[styles.option, isLastOption && styles.lastOption]} onPress={onPress}>
      <View style={_sty.flexSpace}>
        <View style={styles.optionTextContainer}>
          <Text style={styles.optionText}>{label}</Text>
        </View>
        <Ionicons name={icon} size={16} color="rgba(0,0,0,0.35)" />
      </View>
    </RectButton>
  );
}

export function PlTab() {
  return (
    <ImageBackground
      resizeMode={"cover"}
      source={require(GIF_BG)}
      style={_sty.plTab} >
      <View style={_sty.plTabInset} />
      <View style={_sty.plTabInset} />
      <View style={_sty.plTabInset} />
    </ImageBackground>
  );
}

export function PlList() {
  return (
    <View style={_sty.plList}>
      <ImageBackground resizeMode={"cover"} source={require(GIF_BG)}
        style={{ width: 55, height: 55, marginRight: 15 }} />
      <View style={{ width: (Layout.window.width - (55 + 45)), paddingVertical: 3 }}>
        <ImageBackground resizeMode={"cover"} source={require(GIF_BG)}
          style={{ width: '95%', height: 15, marginBottom: 10 }} />
        <ImageBackground resizeMode={"cover"} source={require(GIF_BG)}
          style={{ width: '75%', height: 15 }} />
      </View>
    </View>
  );
}

export const Quantity = ({ qty, updateQty }) => {
  const update = type => {
    switch (type) {
      case 1:
        qty = qty < MAX_QTY ? qty + 1 : qty;
        break;
      default:
        qty = qty > 0 ? qty - 1 : qty;
        break;
    }
    updateQty(qty);
  };

  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableWithoutFeedback onPress={() => update(0)}>
        <Text style={{ padding: 10 }}>-</Text>
      </TouchableWithoutFeedback>
      <Text style={{ padding: 10 }}>{qty}</Text>
      <TouchableWithoutFeedback onPress={() => update(1)}>
        <Text style={{ padding: 10 }}>+</Text>
      </TouchableWithoutFeedback>
    </View>
  );
};

/* export const Quantity = ({ qty, updateQty }) => {
  const update = type => {
    switch (type) {
      case 1:
        qty = qty < MAX_QTY ? qty + 1 : qty;
        break;
      default:
        qty = qty > 0 ? qty - 1 : qty;
        break;
    }
    updateQty(qty);
  };

  return (
    <View style={{ flexDirection: "row" }}>
      <Button onPress={() => update(0)} title="-" />
      <Text style={{ padding: 7 }}>{qty}</Text>
      <Button onPress={() => update(1)} title="+" />
    </View>
  );
}; */

const styles = StyleSheet.create({
  header: {
    height: 40,
    backgroundColor: Colors.bgDefault,
    justifyContent: 'center'
  },
  headerText: {
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 17,
    fontWeight: '500'
  },


  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: 0,
    borderColor: '#ededed',
  },
  lastOption: {
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  optionText: {
    fontSize: 15,
    alignSelf: 'flex-start',
    marginTop: 1,
  },
  optionIconContainer: {
    /* marginRight: 12, */
  },

});