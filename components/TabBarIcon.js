import * as React from 'react';
import { Ionicons, Entypo } from '@expo/vector-icons';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  return (setIcon(props.name, props.focused, props.type));
}

function setIcon(name, focused, type) {
  switch (type) {
    case 'Entypo':
      return <Entypo
        name={name}
        size={25}
        style={{ marginBottom: -3 }}
        color={focused ? Colors.bgDefault : Colors.tabIconDefault}
      />;
      default:
      return <Ionicons
        name={name}
        size={25}
        style={{ marginBottom: -3 }}
        color={focused ? Colors.bgDefault : Colors.tabIconDefault}
      />;
  }
}
