const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: 'tintColor',
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#ffffff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#ffffff',
  bgDefault: '#74AA50',
  white: '#ffffff',
  colorWhite: '#ffffff',
  textDark: '#333',
  textBlack: '#000',
  textWhite: '#ffffff',
  textMute: '#C5C5C5',
  textLight: '#f3f3f3',
  textTitle: '#666',
  borderColor: '#ededed'
};
