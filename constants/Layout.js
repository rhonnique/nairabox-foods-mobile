import { Dimensions, Platform, StatusBar } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const statusBarHeight = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const appBarHeight = Platform.OS === 'ios' ? 44 : 56;

export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
  statusBarHeight,
  appBarHeight,
  statusAppBarHeight: (statusBarHeight + appBarHeight),
  windowPadding: 15,
  windowMargin: 15,
};
