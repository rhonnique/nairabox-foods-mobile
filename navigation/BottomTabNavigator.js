import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import AccountScreen from '../screens/AccountScreen';
import OrdersScreen from '../screens/OrdersScreen';
import Colors from '../constants/Colors';
import { defaultHeaderConfig } from '../components/Components';
import StatesScreen from '../screens/location/StatesScreen';
import CitiesScreen from '../screens/location/CitiesScreen';
import RestaurantsScreen from '../screens/restaurant/Restaurants';
import { AppContext } from '../App';


import TmpRestaurantsScreen from '../screens/tmp/TmpRestaurantsScreen';
import TmpRestaurantDetailsScreen from '../screens/tmp/TmpRestaurantDetailsScreen';


const Tab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';


const TmpsStack = createStackNavigator();

function TmpsStackScreen() {
  return (
    <TmpsStack.Navigator headerMode="none" screenOptions={defaultHeaderConfig}>
      <TmpsStack.Screen name="RestaurantDetailsScreen" component={TmpRestaurantDetailsScreen} />
      <TmpsStack.Screen name="RestaurantsScreen" component={TmpRestaurantsScreen} />
    </TmpsStack.Navigator>
  );
}

const HomeStack = createStackNavigator();

function HomeStackScreen({ navigation, route }) {
  const { currentScreen, setCurrentScreen } = React.useContext(AppContext);
  

  React.useEffect(() => {
    const unsubscribeTabPress = navigation.addListener('tabPress', e => {
      if (currentScreen === 'StateScreen' || currentScreen === 'CityScreen' || currentScreen === 'RestaurantsScreen') {
        e.preventDefault();
      }
    });

    return unsubscribeTabPress;
  }, [navigation, currentScreen]);

  React.useEffect(() => {
    const unsubscribeTabBlur = navigation.addListener('blur', e => {
      console.log(currentScreen, 'tabblur...');
      setCurrentScreen(null);
    });

    return unsubscribeTabBlur;
  }, [navigation, currentScreen]);

  return (
    <HomeStack.Navigator
      screenOptions={defaultHeaderConfig}
    >
      <HomeStack.Screen name="HomeScreen" options={{ title: 'Delivery Location' }} component={HomeScreen} />
      <HomeStack.Screen name="StatesScreen" options={{ title: 'Select State' }} component={StatesScreen} />
      <HomeStack.Screen name="CitiesScreen" options={{ title: 'Select City' }} component={CitiesScreen} />
      <HomeStack.Screen name="RestaurantsScreen" options={{ title: 'Restaurants' }} component={RestaurantsScreen} />
    </HomeStack.Navigator>
  );
}


const OrdersStack = createStackNavigator();

function OrdersStackScreen() {
  return (
    <OrdersStack.Navigator screenOptions={defaultHeaderConfig}>
      <OrdersStack.Screen name="OrdersScreen" options={{ title: 'Orders' }} component={OrdersScreen} />
    </OrdersStack.Navigator>
  );
}

const AccountStack = createStackNavigator();

function AccountStackScreen() {
  return (
    <AccountStack.Navigator screenOptions={defaultHeaderConfig}>
      <AccountStack.Screen name="AccountScreen" options={{ title: 'Account' }} component={AccountScreen} />
    </AccountStack.Navigator>
  );
}

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html

  /* navigation.setOptions({
    headerTitle: getHeaderTitle(route),
    headerTintColor: Colors.textWhite,
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: Colors.bgDefault,
      borderBottomWidth: 0
    }
  }); */

  navigation.setOptions({
    headerShown: false
  })

  return (
    <Tab.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      tabBarOptions={{
        activeTintColor: Colors.bgDefault,
      }}
    >
      <Tab.Screen
        name="Tmps"
        component={TmpsStackScreen}
        options={{
          title: 'Tmps',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-clipboard" />,
        }}
      />

      <Tab.Screen
        name="Orders"
        component={OrdersStackScreen}
        options={{
          title: 'Orders',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-clipboard" />,
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          title: 'Restaurants',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="shop" type="Entypo" />,
        }}
      />
      <Tab.Screen
        name="Account"
        component={AccountStackScreen}

        options={({
          title: 'Account',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-person" />,
        })}


      />
    </Tab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case 'Orders':
      return 'Orders';
    case 'Home':
      return 'Delivery Location';
    case 'Account':
      return 'Account';
  }
}
