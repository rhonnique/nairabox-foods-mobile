import * as React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import {CustomHeader} from '../components/Components';
import { AppContext } from '../App';

export default function Account({ navigation, route }) {
  const { setCurrentScreen } = React.useContext(AppContext);

  /* React.useEffect(() => {
    setCurrentScreen('AccountScreen');
    return () => {
      setCurrentScreen(null);
    }
  }, [route.key]); */

  return (<React.Fragment>
    <View style={{ flex: 1 }} >
      {/* <View style={{height: 40, backgroundColor: 'red', position: 'relative', left: 0, top: 0, zIndex: 999}} /> */}
      {/* <CustomHeader title="Account" /> */}
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <OptionButton
          icon="md-speedometer"
          label="Dashboard"
        />

        <OptionButton
          icon="ios-clipboard"
          label="Recent Orders"
        />

        <OptionButton
          icon="ios-unlock"
          label="Change Password"
        />

        <OptionButton
          icon="ios-settings"
          label="Settings"
          isLastOption
        />
      </ScrollView>
    </View>
  </React.Fragment>
  );
}

/* Account.navigationOptions = ({ navigation }) => {
  return {
    header: (
      <View
        style={{
          height: 45,
          marginTop: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: 'white',
            textAlign: 'center',
            fontWeight: 'bold',
            fontSize: 18,
          }}>
          This is Custom Header
        </Text>
      </View>
    ),
  };
} */

function OptionButton({ icon, label, onPress, isLastOption }) {
  return (
    <RectButton style={[styles.option, isLastOption && styles.lastOption]} onPress={onPress}>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.optionIconContainer}>
          <Ionicons name={icon} size={22} color="rgba(0,0,0,0.35)" />
        </View>
        <View style={styles.optionTextContainer}>
          <Text style={styles.optionText}>{label}</Text>
        </View>
      </View>
    </RectButton>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  contentContainer: {
    paddingTop: 15,
  },
  optionIconContainer: {
    marginRight: 12
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: 0,
    borderColor: '#ededed',
  },
  lastOption: {
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  optionText: {
    fontSize: 15,
    alignSelf: 'flex-start',
    marginTop: 1,
  },
});
