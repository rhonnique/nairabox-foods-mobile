import * as React from 'react';
import {
  Image, Platform, StyleSheet, Text, TouchableOpacity,
  View, ImageBackground, Animated, Easing, Dimensions
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import _sty from "../assets/Styles";
import { Button } from 'react-native-elements';
import { useIsFocused } from '@react-navigation/native';
import Colors from '../constants/Colors';
import { AppContext } from '../App';

export default function HomeScreen({ route, navigation }) {
  const isFocused = useIsFocused();
  const { isData, objectExist, isDataEmpty, getSingleArray, setCurrentScreen, currentScreen,
    savedState, savedCity, getRouteParam, setStateCity } = React.useContext(AppContext);
  const state_id = getRouteParam(route, 'state_id') ? getRouteParam(route, 'state_id') : getSingleArray(savedState, 0);
  const state_name = getRouteParam(route, 'state_name') ? getRouteParam(route, 'state_name') : getSingleArray(savedState, 1);
  const city_id = getRouteParam(route, 'city_id') ? getRouteParam(route, 'city_id') : getSingleArray(savedCity, 0);
  const city_name = getRouteParam(route, 'city_name') ? getRouteParam(route, 'city_name') : getSingleArray(savedCity, 1);

  const [selectedState, setSelectedState] = React.useState([state_id, state_name]);
  const [selectedCity, setSelectedCity] = React.useState([city_id, city_name]);

  /* React.useEffect(() => { 
    if(!isFocused){
      setCurrentScreen('');
      console.log('!isFocused...'); 
    }  
  }, [isFocused]); */

  React.useEffect(() => {
    console.log(state_id, city_id);
  }, []) 

  React.useEffect(() => {
    console.log(state_id, getSingleArray(savedState, 0), 'state_id...');
    setSelectedState([state_id, state_name]);
    setSelectedCity([city_id, city_name]);
    if (state_id && state_name && city_id && city_name) {
      setStateCity([state_id, state_name], [city_id, city_name]);
    }
    console.log(state_id, state_name, city_id, city_name, 'useEffect set both...')
  }, [state_id, state_name, city_id, city_name])

  const search = () => {
    navigation.navigate('RestaurantsScreen', {
      selectedState, selectedCity
    });
  }

  const selectCity = () => {
    if (state_id) {
      navigation.navigate('CitiesScreen', {
        state_id: selectedState[0],
        state_name: selectedState[1]
      });
    }
  }

  

  return (

    <ImageBackground
      imageStyle={styles.containerImageStyle}
      source={require('../assets/images/home-bg.png')}
      style={styles.containerImage}>


      <View style={[_sty.containerFlexCC, _sty.cp]}>


        <View style={_sty.containerForm}>
          <TouchableOpacity
            onPress={() => navigation.navigate('StatesScreen')}
            style={_sty.formSelect}>
            <Text style={_sty.formSelectPlaceHolder}>Select Location</Text>
            <View style={_sty.flexSpace}>
              <Text style={_sty.formSelectText}>{(selectedState ? selectedState[1] : '')}</Text>
              <Ionicons name={'ios-arrow-forward'} size={19} />
            </View>
          </TouchableOpacity>

        </View>

        <View style={_sty.containerForm}>
          <TouchableOpacity
            onPress={() => selectCity()}
            style={_sty.formSelect}>
            <Text style={_sty.formSelectPlaceHolder}>Select City</Text>
            <View style={_sty.flexSpace}>
              <Text style={_sty.formSelectText}>{(selectedCity ? selectedCity[1] : '')}</Text>
              <Ionicons name={'ios-arrow-forward'} size={19} />
            </View>
          </TouchableOpacity>
        </View>

        <Button color={Colors.bgDefault}
          containerStyle={{ width: '100%' }}
          buttonStyle={{ backgroundColor: Colors.bgDefault, height: 45 }}
          titleStyle={{ fontSize: 15 }}
          onPress={() => search()}
          disabled={!city_id}
          title="Search Restaurants"
        />

      </View>


    </ImageBackground>

  );
}

/* HomeScreen.navigationOptions = ({ navigation }) => {
  return { headerShown: false }
}; */

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: '#fff',
  },
  containerImage: {
    flex: 1,
    ...Platform.select({
      ios: {
        zIndex: 1,
      },
      android: {
        elevation: 1,
      },
    })
  },
  containerImageStyle: {
    backgroundColor: '#fff',
    //resizeMode: 'cover',
    flex: 1,
    //top: -150
  },
  containerInner: {
    flex: 1,
    justifyContent: 'center'
  },
  containerSlide: {
    backgroundColor: '#fff',
    position: 'absolute',
    width: '100%',
    height: Dimensions.get('window').height - 70,
    top: 0,
    left: 0,
    zIndex: 3,
    marginTop: 0
  },


  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    })
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
