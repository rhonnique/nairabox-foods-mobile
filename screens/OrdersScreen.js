import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import _sty from "../assets/Styles";
import Colors from '../constants/Colors';
import { EvilIcons } from '@expo/vector-icons';
import { Button } from 'react-native-elements';

export default function OrdersScreen({ navigation }) {

  React.useLayoutEffect(() => {
    navigation.setOptions({

    });
  }, [navigation]);

  return (
    <View style={[_sty.container, _sty.containerFlexCC, _sty.cp]}>
      <EvilIcons name='user' size={100} color={Colors.textMute} style={_sty.mb20} />
      <Button color={Colors.bgDefault}
        containerStyle={{ width: '100%', marginBottom: 20 }}
        buttonStyle={{ backgroundColor: Colors.bgDefault, height: 45 }}
        titleStyle={{ fontSize: 15 }}
        title="Login"
      />
      <Text style={[_sty.colorLightMd, _sty.font16]}>Not yet register? You can when checking out!</Text>
    </View>
  );
}

const styles = StyleSheet.create({

});
