import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Button, TextInput } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import { Feather } from '@expo/vector-icons';
import Colors from '../../constants/Colors';
import { ListView, ErrorDisplay, EmptyDisplay } from '../../components/Components';
import { AppContext } from '../../App';
import { Loader } from '../../components/Components';

export default function StateScreen({ navigation, route }) {
  const { getStates, isData, objectExist, isDataEmpty, setCurrentScreen, 
    currentScreen, } = React.useContext(AppContext);
  const [value, setValue] = React.useState('');
  const [dataFiltered, setDataFiltered] = React.useState(null);
  const [data, setData] = React.useState(null);
  const [dataLoader, setDataLoader] = React.useState(true);

  React.useEffect(() => {
    console.log(route.key, 'StateScreen');
    setCurrentScreen('StateScreen');
    return () => {
      setCurrentScreen(null);
    }
  }, [route.key]);

  React.useEffect(() => {
    fetchData();
    return () => {
      setDataFiltered(null);
      setData(null);
      setDataLoader(true);
    }
  }, []);

  React.useEffect(() => {
    const filter = !dataLoader && dataFiltered && Array.isArray(dataFiltered) &&
      dataFiltered.filter((o) => o.name.match(new RegExp(value, 'i')));
    value.length !== -1 && setData(filter);
  }, [value]);

  const fetchData = () => {
    setData(null);
    setDataLoader(true);
    getStates()
      .then(response => response)
      .then((response) => {
        const { data } = response;
        setDataFiltered(data);
        setData(data);
      })
      .catch((error) => {
        setData({ error: 'true' })
      }).then(() => {
        setDataLoader(false);
      });
  }

  return (
    <View style={_sty.container}>
      {dataLoader && (<Loader />)}
      {!dataLoader && objectExist(data, 'error') && (<ErrorDisplay onPress={() => fetchData()} />)}
      {!dataLoader && data && Array.isArray(data) && (
        <View style={[_sty.searchContainer, _sty.flexSpace]}>
          <TextInput
            style={_sty.searchInput}
            placeholder='Search city'
            placeholderTextColor={Colors.textMute}
            onChangeText={text => setValue(text)}
            value={value}
          />
          <View style={_sty.searchButton}>
            <Feather name='search' size={20}
              color={Colors.bgDefault} style={{ alignSelf: 'center' }} />
          </View>
        </View>)}

      {!dataLoader && isDataEmpty(data) && (<EmptyDisplay />)}

      {isData(data) && data.map((row, index) => {
        return (<ListView key={row._id}
          icon="ios-arrow-forward"
          label={row.name}
          onPress={() => navigation.navigate('CitiesScreen', { state_id: row._id, state_name: row.name })}
          isLastOption
        />);
      })}

    </View>
  );
}

const styles = StyleSheet.create({

});
