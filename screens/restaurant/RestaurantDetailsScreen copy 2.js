import * as React from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl, TouchableOpacity, BackHandler,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Ionicons } from '@expo/vector-icons';
import { AppContext } from '../../App';

const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Layout.statusAppBarHeight;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const initialLayout = { width: Layout.window.width };

import Route1 from './Route1';
import Route2 from './Route2';
import Route3 from './Route3';
import Route4 from './Route4';
import Route5 from './Route5';
import Route6 from './Route6';
import Route7 from './Route7';
import Route8 from './Route8';

import Meal from './Meal';

export default RestaurantDetailsScreen = ({ navigation, route }) => {
  const { setCurrentScreen, restaurantMealsCategory } = React.useContext(AppContext);
  const { restaurantId, name, cuisines, image, delivery } = route.params;
  console.log(restaurantId, 'restaurantId...')

  const [scrollY, setScrollY] = React.useState(new Animated.Value(Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0));
  let { refreshing, setRefreshing } = React.useState(false);
  const [index, setIndex] = React.useState(0);

  const [routes, setRoutes] = React.useState([]);
  const [renderScene, setRenderScene] = React.useState(null);

  /* const renderScene = SceneMap({
    Route1: Route1,
    Route2: Route2,
    Route3: Route3,
    Route4: Route4,
    Route5: Route5,
    Route6: Route6,
    Route7: Route7,
    Route8: Route8,
  }); */

  const _scrollY = Animated.add(
    scrollY,
    Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0
  );

  const headerTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',

  });

  const imageTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const titleScale = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0.8],
    extrapolate: 'clamp',
  });

  const titleTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, -8],
    extrapolate: 'clamp',
  });

  const titleOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const changeIndex = (index) => {
    console.log(index, 'index...')
    setIndex(index);
  }

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: 'white' }}
      style={{ backgroundColor: Colors.bgDefault }}
      scrollEnabled={true}
      tabStyle={{ width: 'auto' }}
    />
  );




  const [mealCats, setMealCats] = React.useState(null);
  const [mealCatsL, setMealCatsL] = React.useState(true);

  const fetchMealCat = (Id) => {
    setMealCatsL(true);
    setMealCats(null);
    restaurantMealsCategory(Id)
      .then(response => response)
      .then(async (response) => {
        let { data } = response;
        let groupCat = [];
        await data.map(dts => {
          if (!groupCat.some(ee => ee._id === dts._id)) {
            groupCat.push(dts);
          }
        });
        setMealCats(groupCat);

      })
      .catch((error) => {
        setMealCats({ error: 'true' });
        setMealCatsL(false);
      }).then(() => {

      });
  }

  React.useEffect(() => {
    fetchMealCat(restaurantId);
  }, []);

  React.useEffect(() => {
    async function setTab() {
      if (mealCats && Array.isArray(mealCats) && mealCats.length > 0) {
        setRenderScene(SceneMap({
          Route1: Route1,
          Route2: Route2,
          Route3: Route3,
          Route4: Route4,
          Route5: Route5,
          Route6: Route6,
          Route7: Route7,
          Route8: Route8,
        }));

        setRoutes([
          { key: 'Route1', title: 'Main Meals' },
          { key: 'Route2', title: 'Popular Products' },
          { key: 'Route3', title: 'Meat' },
          { key: 'Route4', title: 'Grilled Chicken' },
          { key: 'Route5', title: 'Stew & Sauce' },
          { key: 'Route6', title: 'soups' },
          { key: 'Route7', title: 'Sides' },
          { key: 'Route8', title: 'Drinks' },
        ]);
      }
    }
    setTab();
    setMealCatsL(false);

  }, [mealCats]);


  React.useEffect(() => {
    return () => setCurrentScreen('RestaurantsScreen');
  }, []);

  return (

    <View style={_sty.container}>


      {/* <View style={_sty.rdSummaryPlaceholder}>
        <View style={[_sty.rdSummaryContainer, _sty.flexSpace]}>
          <Text style={_sty.rdSummaryText}>3 items</Text>
          <Text style={[_sty.rdSummaryText, _sty.fontWeightBold]}>VIEW ORDER</Text>
          <Text style={_sty.rdSummaryText}>2,950.00</Text>
        </View>
      </View> */}

      <StatusBar
        translucent
        barStyle="light-content"
        backgroundColor="rgba(0, 0, 0, 0.251)"
      />

      <Animated.ScrollView
        style={_sty.container}
        scrollEventThrottle={1}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: true },
        )}

        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => {
              setRefreshing(true);
              setTimeout(() => setRefreshing(false), 1000);
            }}
            progressViewOffset={HEADER_MAX_HEIGHT}
          />
        }

        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}

        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}
      >

        <View style={[_sty.ItemContents, _sty.py15]}>
          <Text style={[_sty.h1, _sty.mb5]}>{name}</Text>
          <Text style={_sty.itemCusines} numberOfLines={2}>{cuisines.join('., ')}</Text>
          <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
        </View>


        {/* Tab container */}
        <View style={[_sty.itemTabContainer]}>
          {!mealCatsL && mealCats && Array.isArray(mealCats) && mealCats.length > 0 && (<TabView
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={changeIndex}
            initialLayout={initialLayout}
          />)}
        </View>
        {/* /Tab container */}




      </Animated.ScrollView>

      <Animated.View
        pointerEvents="none"
        style={[
          styles.header,
          { transform: [{ translateY: headerTranslate }] },
        ]}
      >
        <Animated.Image
          style={[
            styles.backgroundImage,
            {
              opacity: imageOpacity,
              transform: [{ translateY: imageTranslate }],
            },
          ]}
          source={{ uri: image }}
        />

      </Animated.View>

      <View style={[styles.barContainer]} >
        <TouchableOpacity onPress={() => navigation.goBack()} style={_sty.cpx}>
          <Ionicons
            name='ios-arrow-back'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
        <Animated.Text
          style={[styles.barContainerTitle, { opacity: titleOpacity }]}
        >{name}</Animated.Text>
        <TouchableOpacity style={_sty.cpx} onPress={() => navigation.goBack()}>
          <Ionicons
            name='md-more'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
      </View>


    </View>

  );

  //}

}


const styles = StyleSheet.create({

  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.bgDefault,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  barContainer: {
    backgroundColor: 'transparent',
    marginTop: Layout.statusBarHeight,
    height: Layout.appBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  barContainerTitle: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15
  },

  scrollViewContent: {
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },

});