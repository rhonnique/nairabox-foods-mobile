import * as React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ImageBackground, RectButton } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import { Loader, ListView, ErrorDisplay, EmptyDisplay } from '../../components/Components';
import { AppContext } from '../../App';
import { Ionicons, Feather } from '@expo/vector-icons';
import { IMAGE_URL } from 'react-native-dotenv';
import { Asset } from 'expo-asset';

export default function RestaurantsScreen({ navigation, route }) {
  const { getRestaurants, isData, objectExist, isDataEmpty, setCurrentScreen } = React.useContext(AppContext);
  const { selectedState, selectedCity } = route.params;
  const [stateId] = React.useState(selectedState[0]);
  const [stateTitle] = React.useState(selectedState[1]);
  const [cityId] = React.useState(selectedCity[0]);
  const [cityTitle] = React.useState(selectedCity[1]);
  const [dataLoader, setDataLoader] = React.useState(true);
  const [data, setData] = React.useState(null);
  const [dataFiltered, setDataFiltered] = React.useState(null);


  React.useEffect(() => {
    setCurrentScreen('RestaurantsScreen');
    return () => {
      setCurrentScreen(null);
      setDataFiltered(null);
      setData(null);
      setDataLoader(true);
    }
  }, [route.key]);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: props => (<View style={[_sty.containerFlexCC]}>
        <Text style={[_sty.font11, _sty.colorWhite]}>Delivering to</Text>
        <Text style={[_sty.font14, _sty.colorWhite, _sty.fontWeight600]}>{cityTitle}, {stateTitle}</Text>
      </View>),
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.goBack()} style={_sty.cpl}>
          <Ionicons
            name='ios-arrow-back'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity style={_sty.cpr}>
          <Feather
            name='search'
            size={22}
            color='#fff'
          />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);


  const fetchData = (cityId) => {
    setDataLoader(true);
    setData(null);
    getRestaurants(cityId)
      .then(response => response)
      .then((response) => {
        const { data } = response;
        //console.log(data, 'data....')
        setDataFiltered(data);
        setData(data);
      })
      .catch((error) => {
        setData({ error: 'true' })
      }).then(() => {
        setDataLoader(false);
      });
  }

  React.useEffect(() => {
    fetchData(selectedCity[0]);
  }, []);

  React.useEffect(() => {
    fetchData(selectedCity[0]);
  }, []);


  return (
    <View style={_sty.container}>
      {dataLoader && (<Loader />)}
      {!dataLoader && objectExist(data, 'error') && (<ErrorDisplay onPress={() => fetchData()} />)}
      {!dataLoader && isDataEmpty(data) && (<EmptyDisplay />)}

      {isData(data) && (
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* *********** Featured view *********** */}
          <View style={[_sty.featuredContainer, _sty.py15]}>
            <Text style={[_sty.h6, _sty.px10]}>Featured Restaurant</Text>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal={true}>
              <TouchableOpacity style={_sty.mx5}>
                <ImageBackground
                  style={_sty.featuredItemImage}
                  source={require('../../tmp/images/res1.png')}
                />
                <Text
                  style={_sty.featuredItemTitle}
                  numberOfLines={2} >Sweet Sensation - Ikoyi</Text>
              </TouchableOpacity>

              <TouchableOpacity style={_sty.mx5}>
                <ImageBackground
                  style={_sty.featuredItemImage}
                  source={require('../../tmp/images/res2.jpg')}
                />
                <Text
                  style={_sty.featuredItemTitle}
                  numberOfLines={2} >Shiro</Text>
              </TouchableOpacity>

              <TouchableOpacity style={_sty.mx5}>
                <ImageBackground
                  style={_sty.featuredItemImage}
                  source={require('../../tmp/images/res3.jpg')}
                />
                <Text
                  style={_sty.featuredItemTitle}
                  numberOfLines={2} >Chicken Republic - Awolowo 2</Text>
              </TouchableOpacity>

              <TouchableOpacity style={_sty.mx5}>
                <ImageBackground
                  style={_sty.featuredItemImage}
                  source={require('../../tmp/images/res4.jpg')}
                />
                <Text
                  style={_sty.featuredItemTitle}
                  numberOfLines={2} >The Place - Ikoyi</Text>
              </TouchableOpacity>

              <TouchableOpacity style={_sty.mx5}>
                <ImageBackground
                  style={_sty.featuredItemImage}
                  source={require('../../tmp/images/res5.jpg')}
                />
                <Text
                  style={_sty.featuredItemTitle}
                  numberOfLines={2} >KFC - Adeola Odeku</Text>
              </TouchableOpacity>

              <TouchableOpacity style={_sty.mx5}>
                <ImageBackground
                  style={_sty.featuredItemImage}
                  source={require('../../tmp/images/res6.jpg')}
                />
                <Text
                  style={_sty.featuredItemTitle}
                  numberOfLines={2} >Pizza Hut - Ikoyi</Text>
              </TouchableOpacity>

            </ScrollView>
          </View>
          {/* *********** /Featured view *********** */}

          {data.map((row, index) => {
            let { _id, name, cuisines, image, delivery } = row;
            cuisines = row.cuisines.map(c => c.cuisine.name);
            image = `${IMAGE_URL}/md/${image}`;
            console.log(_id, name, cuisines, image, delivery);

            return (<TouchableOpacity key={row._id}
              style={_sty.itemContainer}
              onPress={() => navigation.navigate('RestaurantDetailsScreen', {
                restaurantId: _id, name, cuisines, image, delivery
              })}
            >
              <ImageBackground
                style={_sty.ItemImage}
                source={{ uri: image }}>
                <View style={_sty.ItemImageContentBottom}>
                  <Text style={_sty.itemDeliveryTime}>{delivery} MIN</Text>
                </View>
              </ImageBackground>
              <View style={_sty.ItemContents}>
                <Text style={[_sty.h1, _sty.mb5]}>{name}</Text>
                <Text style={_sty.itemCusines} numberOfLines={2}>{cuisines.join(', ')}</Text>
                <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
              </View>
            </TouchableOpacity>);
          })}







        </ScrollView>
      )}
    </View>
  );
}

const styles = StyleSheet.create({

});
