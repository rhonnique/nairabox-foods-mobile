import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* Main Meals */
export default Route1 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Beans Pottage</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Crispy Fried Yam</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>16</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Spaghetti</Text>
          <Text style={[_sty.listItemPrice]}>N550</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Village Rice</Text>
          <Text style={[_sty.listItemPrice]}>N700</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>2</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Yamarita</Text>
          <Text style={[_sty.listItemPrice]}>N200</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Fried Rice</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Jambolaya Rice</Text>
          <Text style={[_sty.listItemPrice]}>N800</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Jollof Rice</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Ofada Rice</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Prawns Rice</Text>
          <Text style={[_sty.listItemPrice]}>N800</Text>
        </View>
      </TouchableOpacity>
    </ScrollView>
  );