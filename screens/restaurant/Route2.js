import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* Popular Products */
export default Route2 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Asun</Text>
          <Text style={[_sty.listItemPrice]}>N1,200</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Edikaikong Soup</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>16</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Goat Meat Peppersoup</Text>
          <Text style={[_sty.listItemPrice]}>N550</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Snail</Text>
          <Text style={[_sty.listItemPrice]}>N950</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>2</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Stir Fry Pasta</Text>
          <Text style={[_sty.listItemPrice]}>N600</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Asun</Text>
          <Text style={[_sty.listItemPrice]}>N1,200</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Edikaikong Soup</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>16</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Goat Meat Peppersoup</Text>
          <Text style={[_sty.listItemPrice]}>N550</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Snail</Text>
          <Text style={[_sty.listItemPrice]}>N950</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>2</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Stir Fry Pasta</Text>
          <Text style={[_sty.listItemPrice]}>N600</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Dried Pineapple (100g)</Text>
          <Text style={[_sty.listItemPrice]}>N600</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Dried Pineapple (50g)</Text>
          <Text style={[_sty.listItemPrice]}>N400</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>1</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Pasta Vegetable Salad</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Special Fried Rice</Text>
          <Text style={[_sty.listItemPrice]}>N700</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Titus Fish</Text>
          <Text style={[_sty.listItemPrice]}>N600</Text>
        </View>
      </TouchableOpacity>





      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Stir Fried Spaghetti</Text>
          <Text style={[_sty.listItemPrice]}>N600</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Fruit Nut Mix (40g)</Text>
          <Text style={[_sty.listItemPrice]}>N450</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Dried Mango</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Dried Cashew Nuts (40g)</Text>
          <Text style={[_sty.listItemPrice]}>N450</Text>
        </View>
      </TouchableOpacity>



    </ScrollView>
  );