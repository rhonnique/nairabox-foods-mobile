import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* Meat */
export default Route3 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Beef</Text>
          <Text style={[_sty.listItemPrice]}>N3500</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Fresh Croacker Fish</Text>
          <Text style={[_sty.listItemPrice]}>N1,050</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Gizzard</Text>
          <Text style={[_sty.listItemPrice]}>N400</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Grilled Chicken</Text>
          <Text style={[_sty.listItemPrice]}>N1000</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Pepper Chicken</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Prawn in Pepper</Text>
          <Text style={[_sty.listItemPrice]}>N2,500</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Ponmo</Text>
          <Text style={[_sty.listItemPrice]}>N250</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>16</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Grilled Turkey</Text>
          <Text style={[_sty.listItemPrice]}>N900</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Ofada Meat</Text>
          <Text style={[_sty.listItemPrice]}>N700</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>2</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Pepper Ram Meat</Text>
          <Text style={[_sty.listItemPrice]}>N1,00</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Prawn in Batter</Text>
          <Text style={[_sty.listItemPrice]}>N800</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Shaki</Text>
          <Text style={[_sty.listItemPrice]}>N300</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Croaker Fish- Fried</Text>
          <Text style={[_sty.listItemPrice]}>N1,100</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Croaker Fish- Stewed</Text>
          <Text style={[_sty.listItemPrice]}>N1,100</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Small Grilled Chicken</Text>
          <Text style={[_sty.listItemPrice]}>N550</Text>
        </View>
      </TouchableOpacity>





      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Turkey Wings</Text>
          <Text style={[_sty.listItemPrice]}>N1,000</Text>
        </View>
      </TouchableOpacity>



    </ScrollView>
  );