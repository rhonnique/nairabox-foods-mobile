import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* Grilled Chicken */
export default Route4 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Barbecue Chicken</Text>
          <Text style={[_sty.listItemPrice]}>N450</Text>
        </View>
      </TouchableOpacity>
      
    </ScrollView>
  );