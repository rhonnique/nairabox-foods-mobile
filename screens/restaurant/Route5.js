import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* Stew & Sauce */
export default Route5 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Egg in Sauce</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Fish Sauce</Text>
          <Text style={[_sty.listItemPrice]}>N500</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Egg Sauce</Text>
          <Text style={[_sty.listItemPrice]}>N400</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Ofada Beef Sauce</Text>
          <Text style={[_sty.listItemPrice]}>N700</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Plantain Meat Sauce</Text>
          <Text style={[_sty.listItemPrice]}>N600</Text>
        </View>
      </TouchableOpacity>
    </ScrollView>
  );