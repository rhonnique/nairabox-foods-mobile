import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* soups */
export default Route6 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Efo Riro Soup</Text>
          <Text style={[_sty.listItemPrice]}>N450</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Egusi Soup</Text>
          <Text style={[_sty.listItemPrice]}>N400</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>16</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Gbegiri</Text>
          <Text style={[_sty.listItemPrice]}>N100</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Ewedu</Text>
          <Text style={[_sty.listItemPrice]}>N100</Text>
        </View>
      </TouchableOpacity>
      
    </ScrollView>
  );