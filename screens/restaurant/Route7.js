import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";

/* Sides */
export default Route7 = () => (
    <ScrollView>
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Fried Plantain</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Moi Moi Elewe</Text>
          <Text style={[_sty.listItemPrice]}>N300</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Moi Moi in Pack</Text>
          <Text style={[_sty.listItemPrice]}>N300</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Boiled Yam</Text>
          <Text style={[_sty.listItemPrice]}>N200</Text>
        </View>
        <View style={[_sty.containerFlexCC, _sty.listItemBadge]}>
          <Text style={[_sty.listItemBadgeText]}>2</Text>
        </View>
      </TouchableOpacity>
  
      <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
        <View style={[_sty.listItem]}>
          <Text style={[_sty.listItemTitle]}>Crispy Yam</Text>
          <Text style={[_sty.listItemPrice]}>N350</Text>
        </View>
      </TouchableOpacity>
    </ScrollView>
  );