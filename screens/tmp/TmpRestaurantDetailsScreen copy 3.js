import * as React from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl, TouchableOpacity, BackHandler,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { Ionicons } from '@expo/vector-icons';
import { AppContext } from '../../App';

const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Layout.statusAppBarHeight;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const initialLayout = { width: Layout.window.width };

import Meal from './../restaurant/Meal';

export default RestaurantDetailsScreen = ({ navigation, route }) => {
  const { setCurrentScreen, restaurantMealsCategory, makeId } = React.useContext(AppContext);
  //const { restaurantId, name, cuisines, image, delivery } = route.params;
  //console.log(restaurantId, 'restaurantId...')

  const restaurantId = '5c88d8ff4652fe7eec52092d';
  const name = 'Bukka Hut - Lekki';
  const cuisines = ["African", "Nigerian"];
  const image = 'http://173.230.149.104:8884/uploads/md/1557585772238_GdAlaLDxssfK.png';
  const delivery = '35-45';

  const [scrollY, setScrollY] = React.useState(new Animated.Value(Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0));
  let { refreshing, setRefreshing } = React.useState(false);
  const [index, setIndex] = React.useState(0);
  const [tabRoute, setTabRoute] = React.useState('');

  const [routes, setRoutes] = React.useState([]);
  const [mealData, setMealData] = React.useState([]);
  const [renderScene, setRenderScene] = React.useState({});

  const _scrollY = Animated.add(
    scrollY,
    Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0
  );

  const headerTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',

  });

  const imageTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const titleScale = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0.8],
    extrapolate: 'clamp',
  });

  const titleOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const Meals = ({ index, jumpTo }) => {
    const [load, setLoad] = React.useState(false);
    //console.log(index, jumpTo, 'Meals route...');
    React.useEffect(() => {
      console.log(tabRoute, 'jumpTo changed...');
    }, [jumpTo, tabRoute]);
    return (
      <ScrollView>
        <Text>{tabRoute}</Text>
        <Text>{index === 0 ? 'loading..' : 'wait.'}</Text>
      </ScrollView>
    )
  };

  const [mealCats, setMealCats] = React.useState(null);
  const [mealCatsL, setMealCatsL] = React.useState(true);

  const fetchMealCat = (Id) => {
    setMealCatsL(true);
    setMealCats(null);
    restaurantMealsCategory(Id)
      .then(response => response)
      .then(async (response) => {
        let { data } = response;
        let groupCat = [];
        await data.map(dts => {
          if (!groupCat.some(ee => ee._id === dts._id)) {
            groupCat.push(dts);
          }
        });

        let createScene = {};
        let createRoute = [];
        await groupCat.map((dts, index) => {
          Object.assign(createScene, {
            [`${dts._id}`]: ({ route, jumpTo }) =>
              <Meals index={index} route={route} jumpTo={jumpTo}/>
          });
          createRoute.push({
            index,
            key: `${dts._id}`,
            title: dts.name,
            data: null,
            loading: null
          });
        });

        //console.log(createScene, createRoute, 'createScene...');

        setRoutes(createRoute);
        setRenderScene(createScene);

        //({ route }) => <LazyPlaceholder route={route} />

        /* setRoutes([
          { key: 'first', title: 'First' },
          { key: 'second', title: 'Second' },
        ]);
        setRenderScene({
          first: FirstRoute,
          second: SecondRoute,
        }); */

        setMealCats(groupCat);
        setMealCatsL(false);

      })
      .catch((error) => {
        setMealCats({ error: 'true' });
        setMealCatsL(false);
      }).then(() => {

      });
  }

  React.useEffect(() => {
    fetchMealCat(restaurantId);
    return () => setCurrentScreen('RestaurantsScreen');
  }, []);


  const HomeScreen = ({ navigation, route }) => {
    const { index } = route.params;
    const [routeData, setRouteData] = React.useState(routes[index]);
    const [load, setLoad] = React.useState('waiting...');
    const [data, setData] = React.useState(false);
    React.useEffect(() => {
      const unsubscribe = navigation.addListener('focus', e => {
        //initRouteData(routeData);
        /* console.log('start loading...', index)
        setLoad(true); 
        if (!data) {
          setLoad('loading...')
          setTimeout(() => {
            setLoad('loaded');
            setData(true);
          }, 5000);
        }
        */
      });

      return unsubscribe;
    }, [navigation]);

    React.useEffect(() => {
      //setLoad(index === 0 ? true : false);
      if (index === 0) {
        /* if (!data) {
          setLoad('loading...')
          setTimeout(() => {
            setLoad('loaded');
            setData(true);
          }, 5000);
        } */
        initRouteData(index);
      }
    }, []);

    const initRouteData = (index) => {
      let current = routes.find(route => route.index === index);
      current.loading = true;
      setRoutes(routes.map(data => data.index === index ? current : data)); 
      /* routes.map(data => {
        if (data.index === index) {
          console.log(data, 'initRouteData...');
        }
      }) */
      //setRoutes(routes.map(item => item.index === index ? {...item, loading : true} : item ))
      //setRoutes(routes.map(item => item.index === index ? {...item, loading : true} : item ))
      //let routeData = [...routes];

      /* if (!routeData.data) {
        setRouteData({ ...routeData, loading: true });
        setTimeout(() => {
          setRouteData({ ...routeData, loading: false });
          setRouteData({ ...routeData, data: true });
        }, 5000);
      } */
    }

    /* React.useEffect(() => {
      console.log(routes, 'routes....')
    }, [routes]); */


    /* const initRouteData = (routeData, index) => {
      if (!routeData.data) {
        setRouteData({ ...routeData, loading: true });
        setTimeout(() => {
          setRouteData({ ...routeData, loading: false });
          setRouteData({ ...routeData, data: true });
        }, 5000);
      }
    } */

    return (
      <View style={{ flex: 1, }}>
        <Text>{JSON.stringify(routes[index])}</Text>
        <Text>-------------------------</Text>
        {routeData.loading && (<Text>loading...</Text>)}
        {!routeData.loading && routeData.data && (<Text>data loaded</Text>)}

      </View>
    );
  }

  const Tab = createMaterialTopTabNavigator();

  return (

    <View style={_sty.container}>


      {/* <View style={_sty.rdSummaryPlaceholder}>
        <View style={[_sty.rdSummaryContainer, _sty.flexSpace]}>
          <Text style={_sty.rdSummaryText}>3 items</Text>
          <Text style={[_sty.rdSummaryText, _sty.fontWeightBold]}>VIEW ORDER</Text>
          <Text style={_sty.rdSummaryText}>2,950.00</Text>
        </View>
      </View> */}

      <StatusBar
        translucent
        barStyle="light-content"
        backgroundColor="rgba(0, 0, 0, 0.251)"
      />

      <Animated.ScrollView
        style={_sty.container}
        scrollEventThrottle={1}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: true },
        )}

        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}

        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}
      >

        <View style={[_sty.ItemContents, _sty.py15]}>
          <Text style={[_sty.h1, _sty.mb5]}>{name}</Text>
          <Text style={_sty.itemCusines} numberOfLines={2}>{cuisines.join('., ')}</Text>
          <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
        </View>


        {/* ************* Tab container ******************* */}
        <View style={[_sty.itemTabContainer]}>
          {!mealCatsL && mealCats && Array.isArray(mealCats) && mealCats.length > 0 && (
            <NavigationContainer
              independent={true}
            >
              <Tab.Navigator
                tabBarOptions={{
                  labelStyle: { fontSize: 12 },
                  tabStyle: { width: 'auto' },
                  style: { backgroundColor: 'powderblue' },
                  scrollEnabled: true,
                  initialLayout: initialLayout
                }}

              >
                {routes.map((dt, index) => {
                  return (<React.Fragment>
                    <Tab.Screen
                      key={dt.key}
                      initialParams={{ index }}
                      name={dt.key}
                      options={{ title: dt.title }}
                      component={HomeScreen} />
                  </React.Fragment>)
                })}
              </Tab.Navigator>
            </NavigationContainer>)}
        </View>
        {/* /Tab container */}




      </Animated.ScrollView>

      <Animated.View
        pointerEvents="none"
        style={[
          styles.header,
          { transform: [{ translateY: headerTranslate }] },
        ]}
      >
        <Animated.Image
          style={[
            styles.backgroundImage,
            {
              opacity: imageOpacity,
              transform: [{ translateY: imageTranslate }],
            },
          ]}
          source={{ uri: image }}
        />

      </Animated.View>

      <View style={[styles.barContainer]} >
        <TouchableOpacity onPress={() => navigation.goBack()} style={_sty.cpx}>
          <Ionicons
            name='ios-arrow-back'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
        <Animated.Text
          style={[styles.barContainerTitle, { opacity: titleOpacity }]}
        >{name}</Animated.Text>
        <TouchableOpacity style={_sty.cpx} onPress={() => navigation.goBack()}>
          <Ionicons
            name='md-more'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
      </View>


    </View>

  );

  //}

}


const styles = StyleSheet.create({

  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.bgDefault,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  barContainer: {
    backgroundColor: 'transparent',
    marginTop: Layout.statusBarHeight,
    height: Layout.appBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  barContainerTitle: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15
  },

  scrollViewContent: {
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },

});