import * as React from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl, TouchableOpacity, ImageBackground, Image
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { PlTab, PlList } from '../../components/Components'

import { Ionicons } from '@expo/vector-icons';
import { AppContext } from '../../App';

const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Layout.statusAppBarHeight;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const initialLayout = { width: Layout.window.width };

import Meal from './../restaurant/Meal';

export default RestaurantDetailsScreen = ({ navigation, route }) => {
  const { setCurrentScreen, restaurantMealsCatGroup, restaurantMealsCat } = React.useContext(AppContext);
  //const { restaurantId, name, cuisines, image, delivery } = route.params;
  //console.log(restaurantId, 'restaurantId...')

  const restaurantId = '5c88d8ff4652fe7eec52092d';
  const name = 'Bukka Hut - Lekki';
  const cuisines = ["African", "Nigerian"];
  const image = 'http://173.230.149.104:8884/uploads/md/1557585772238_GdAlaLDxssfK.png';
  const delivery = '35-45';

  const [scrollY, setScrollY] = React.useState(new Animated.Value(Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0));
  let { refreshing, setRefreshing } = React.useState(false);
  const [index, setIndex] = React.useState(0);
  const [tabRoute, setTabRoute] = React.useState('');

  const [routes, setRoutes] = React.useState([]);
  const [mealsData, setMealsData] = React.useState([]);
  const [renderScene, setRenderScene] = React.useState({});

  const _scrollY = Animated.add(
    scrollY,
    Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0
  );

  const headerTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',

  });

  const imageTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const titleScale = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0.8],
    extrapolate: 'clamp',
  });

  const titleOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const Meals = ({ index, jumpTo }) => {
    const [load, setLoad] = React.useState(false);
    //console.log(index, jumpTo, 'Meals route...');
    React.useEffect(() => {
      console.log(tabRoute, 'jumpTo changed...');
    }, [jumpTo, tabRoute]);
    return (
      <ScrollView>
        <Text>{tabRoute}</Text>
        <Text>{index === 0 ? 'loading..' : 'wait.'}</Text>
      </ScrollView>
    )
  };

  const [mealCatsGroupLoader, setMealCatsGroupLoader] = React.useState(true);

  const fetchMealCat = (Id) => {
    restaurantMealsCatGroup(Id)
      .then(response => response)
      .then(async (response) => {
        let { data } = response;
        let groupCat = [];
        await data.map(dts => {
          if (!groupCat.some(ee => ee._id === dts._id)) {
            groupCat.push(dts);
          }
        });

        let createRoute = [];
        await groupCat.map((dts, index) => {
          createRoute.push({
            index,
            key: `${dts._id}`,
            title: dts.name,
            data: null,
            loading: true
          });
        });

        setRoutes(createRoute);
        setMealsData(createRoute);
        setMealCatsGroupLoader(false);

      })
      .catch((error) => {
        setMealCats({ error: 'true' });
        setMealCatsGroupLoader(false);
      }).then(() => {

      });
  }

  /* const fetchMeal = (index, catId) => {
    restaurantMealsCat(restaurantId, catId)
      .then(response => response)
      .then(async (response) => {
        let { data } = response;
        let current = [...mealsData];
        current[index] = { ...mealsData[index], loading: false, data };
        setMealsData(current);
      })
      .catch((error) => {
        
      }).then(() => {

      });
  } */

  const fetchMeal = (index, current, catId) => {
    restaurantMealsCat(restaurantId, catId)
      .then(response => response)
      .then(async (response) => {
        let { data } = response;
        //let current = [...mealsData];
        current[index] = { ...mealsData[index], loading: false, data };
        setMealsData(current);
      })
      .catch((error) => {
        
      }).then(() => {

      });
  }

  React.useEffect(() => {
    fetchMealCat(restaurantId);
    return () => setCurrentScreen('RestaurantsScreen');
  }, []);


  const HomeScreen = ({ navigation, route }) => {
    const { index, catId } = route.params;
    console.log(navigation, route, 'route...');
    const [mealData, setMealData] = React.useState(mealsData[index]);
    const [load, setLoad] = React.useState('waiting...');
    React.useEffect(() => {
      const unsubscribe = navigation.addListener('focus', e => {
        initRouteData(index);
      });

      return unsubscribe;
    }, [navigation]);

    React.useEffect(() => {
      if (index === 0)
        initRouteData(index);
    }, []);

    const initRouteData = (index) => {
      let current = [...mealsData];
      if (!current[index].data /* && !current[index].loading */) {
        /* current[index] = { ...mealsData[index], loading: true };
        setMealsData(current); */

        /* setTimeout(() => {
          let current2 = [...mealsData];
          current2[index] = { ...mealsData[index], loading: false, data: true };
          setMealsData(current2);
        }, 5000); */


      }
    }

    return (
      <View style={_sty.container}>
        {mealData.loading && (<PlList />)}
        {mealData.data && (<ScrollView>
          <TouchableOpacity style={[_sty.listItemContainer, _sty.flexSpace, _sty.cpx]}>
            <View style={[_sty.listItem]}>
              <Text style={[_sty.listItemTitle]}>Beans Pottage</Text>
              <Text style={[_sty.listItemPrice]}>N350</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>)}
      </View>
    );
  }



  const Tab = createMaterialTopTabNavigator();

  return (

    <View style={_sty.container}>


      {/* <View style={_sty.rdSummaryPlaceholder}>
        <View style={[_sty.rdSummaryContainer, _sty.flexSpace]}>
          <Text style={_sty.rdSummaryText}>3 items</Text>
          <Text style={[_sty.rdSummaryText, _sty.fontWeightBold]}>VIEW ORDER</Text>
          <Text style={_sty.rdSummaryText}>2,950.00</Text>
        </View>
      </View> */}

      <StatusBar
        translucent
        barStyle="light-content"
        backgroundColor="rgba(0, 0, 0, 0.251)"
      />

      <Animated.ScrollView
        style={_sty.container}
        scrollEventThrottle={1}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: true },
        )}

        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}

        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}
      >

        <View style={[_sty.ItemContents, _sty.py15]}>
          <Text style={[_sty.h1, _sty.mb5]}>{name}</Text>
          <Text style={_sty.itemCusines} numberOfLines={2}>{cuisines.join('., ')}</Text>
          <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
        </View>


        {/* ************* Tab container ******************* */}
        <View style={[_sty.itemTabContainer]}>
          {mealCatsGroupLoader && (<PlTab />)}

          {!mealCatsGroupLoader && routes && Array.isArray(routes) && routes.length > 0 && (
            <NavigationContainer
              independent={true}
            >
              <Tab.Navigator
                tabBarOptions={{
                  labelStyle: { fontSize: 14, fontWeight: '500', color: '#fff' },
                  tabStyle: { width: 'auto' },
                  style: { backgroundColor: Colors.bgDefault },
                  scrollEnabled: true,
                  initialLayout: initialLayout
                }}

              >
                {routes.map((dt, index) => {
                  return (<Tab.Screen
                    key={dt.key}
                    initialParams={{ index, mealId: dt.key }}
                    name={dt.key}
                    options={{ title: dt.title }}
                    component={HomeScreen} />)
                })}
              </Tab.Navigator>
            </NavigationContainer>)}
        </View>
        {/* /Tab container */}




      </Animated.ScrollView>

      <Animated.View
        pointerEvents="none"
        style={[
          styles.header,
          { transform: [{ translateY: headerTranslate }] },
        ]}
      >
        <Animated.Image
          style={[
            styles.backgroundImage,
            {
              opacity: imageOpacity,
              transform: [{ translateY: imageTranslate }],
            },
          ]}
          source={{ uri: image }}
        />

      </Animated.View>

      <View style={[styles.barContainer]} >
        <TouchableOpacity onPress={() => navigation.goBack()} style={_sty.cpx}>
          <Ionicons
            name='ios-arrow-back'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
        <Animated.Text
          style={[styles.barContainerTitle, { opacity: titleOpacity }]}
        >{name}</Animated.Text>
        <TouchableOpacity style={_sty.cpx} onPress={() => navigation.goBack()}>
          <Ionicons
            name='md-more'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
      </View>


    </View>

  );

  //}

}


const styles = StyleSheet.create({

  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.bgDefault,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  barContainer: {
    backgroundColor: 'transparent',
    marginTop: Layout.statusBarHeight,
    height: Layout.appBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  barContainerTitle: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15
  },

  scrollViewContent: {
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },

});