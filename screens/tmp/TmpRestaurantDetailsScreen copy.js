import React, { Component, useEffect, useState } from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl, TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Ionicons } from '@expo/vector-icons';

const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Layout.statusAppBarHeight;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const initialLayout = { width: Layout.window.width };

import Route1 from '../../screens/restaurant/Route1';
import Route2 from '../../screens/restaurant/Route2';
import Route3 from '../../screens/restaurant/Route3';
import Route4 from '../../screens/restaurant/Route4';
import Route5 from '../../screens/restaurant/Route5';
import Route6 from '../../screens/restaurant/Route6';
import Route7 from '../../screens/restaurant/Route7';
import Route8 from '../../screens/restaurant/Route8';

const TmpRestaurantDetailsScreen = () => {
  const [scrollY, setScrollY] = useState(new Animated.Value(Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0));
  let { refreshing, setRefreshing } = useState(false);
  const [index, setIndex] = React.useState(0);

  const [routes] = React.useState([
    { key: 'Route1', title: 'Main Meals' },
    { key: 'Route2', title: 'Popular Products' },
    { key: 'Route3', title: 'Meat' },
    { key: 'Route4', title: 'Grilled Chicken' },
    { key: 'Route5', title: 'Stew & Sauce' },
    { key: 'Route6', title: 'soups' },
    { key: 'Route7', title: 'Sides' },
    { key: 'Route8', title: 'Drinks' },
  ]);

  const renderScene = SceneMap({
    Route1: Route1,
    Route2: Route2,
    Route3: Route3,
    Route4: Route4,
    Route5: Route5,
    Route6: Route6,
    Route7: Route7,
    Route8: Route8,
  });

  useEffect(() => {
  }, []);

  const _scrollY = Animated.add(
    scrollY,
    Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0
  );

  const headerTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',

  });

  const imageTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const titleScale = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0.8],
    extrapolate: 'clamp',
  });

  const titleTranslate = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, -8],
    extrapolate: 'clamp',
  });

  const titleOpacity = _scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const changeIndex = (index) => {
    console.log(index, 'index...')
    setIndex(index);
  }

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: 'white' }}
      style={{ backgroundColor: Colors.bgDefault }}
      scrollEnabled={true}
      tabStyle={{ width: 'auto' }}
    />
  );

  return (

    <View style={_sty.container}>


      {/* <View style={_sty.rdSummaryPlaceholder}>
        <View style={[_sty.rdSummaryContainer, _sty.flexSpace]}>
          <Text style={_sty.rdSummaryText}>3 items</Text>
          <Text style={[_sty.rdSummaryText, _sty.fontWeightBold]}>VIEW ORDER</Text>
          <Text style={_sty.rdSummaryText}>2,950.00</Text>
        </View>
      </View> */}

      <StatusBar
        translucent
        barStyle="light-content"
        backgroundColor="rgba(0, 0, 0, 0.251)"
      />

      <Animated.ScrollView
        style={_sty.container}
        scrollEventThrottle={1}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: true },
        )}

        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => {
              setRefreshing(true);
              setTimeout(() => setRefreshing(false), 1000);
            }}
            progressViewOffset={HEADER_MAX_HEIGHT}
          />
        }

        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}

        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}
      >

        <View style={[_sty.ItemContents, _sty.py15]}>
          <Text style={[_sty.h1, _sty.mb5]}>Brooklyn Bistro Integrated Services</Text>
          <Text style={_sty.itemCusines} numberOfLines={2}>African, Breakfast, Sandwiches, Salads, Vegetarian</Text>
          <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
        </View>


        {/* Tab container */}
        <View style={[_sty.itemTabContainer]}>
          <TabView
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={changeIndex}
            initialLayout={initialLayout}
          />
        </View>
        {/* /Tab container */}




      </Animated.ScrollView>

      <Animated.View
        pointerEvents="none"
        style={[
          styles.header,
          { transform: [{ translateY: headerTranslate }] },
        ]}
      >
        <Animated.Image
          style={[
            styles.backgroundImage,
            {
              opacity: imageOpacity,
              transform: [{ translateY: imageTranslate }],
            },
          ]}
          source={require('../../tmp/images/res1.png')}
        />

      </Animated.View>

      <View style={[styles.barContainer, _sty.cpx]} >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons
            name='ios-arrow-back'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
        <Animated.Text
          style={[styles.barContainerTitle, { opacity: titleOpacity }]}
        >Brooklyn Bistro Integrated Services</Animated.Text>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons
            name='md-more'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
      </View>


    </View>

  );

  //}

}

export default TmpRestaurantDetailsScreen;

const styles = StyleSheet.create({

  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.bgDefault,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  barContainer: {
    backgroundColor: 'transparent',
    marginTop: Layout.statusBarHeight,
    height: Layout.appBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  barContainerTitle: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15
  },

  scrollViewContent: {
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },

});