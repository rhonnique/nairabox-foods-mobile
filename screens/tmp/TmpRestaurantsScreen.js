import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Button, ImageBackground } from 'react-native';
import { SimpleLineIcons, Feather, Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
import _sty from "../../assets/Styles";
import { AppContext } from '../../App';

export default function TmpRestaurantsScreen({ navigation, route }) {
  const { setCurrentScreen } = React.useContext(AppContext);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: props => (<View style={[_sty.containerFlexCC]}>
        <Text style={[_sty.font11, _sty.colorWhite]}>Delivering to</Text>
        <Text style={[_sty.font14, _sty.colorWhite, _sty.fontWeight600]}>Ikoyi, Lagos</Text>
      </View>),
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.goBack()} style={_sty.cpl}>
          <Ionicons
            name='ios-arrow-back'
            size={24}
            color='#fff'
          />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity style={_sty.cpr}>
          <Feather
            name='search'
            size={22}
            color='#fff'
          />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <View style={[styles.container]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* Featured view */}
        <View style={[_sty.featuredContainer, _sty.py15]}>
          <Text style={[_sty.h6, _sty.px10]}>Featured Restaurant</Text>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            horizontal={true}>
            <TouchableOpacity style={_sty.mx5}>
              <ImageBackground
                style={_sty.featuredItemImage}
                source={require('../../tmp/images/res1.png')}
              />
              <Text
                style={_sty.featuredItemTitle}
                numberOfLines={2} >Sweet Sensation - Ikoyi</Text>
            </TouchableOpacity>

            <TouchableOpacity style={_sty.mx5}>
              <ImageBackground
                style={_sty.featuredItemImage}
                source={require('../../tmp/images/res2.jpg')}
              />
              <Text
                style={_sty.featuredItemTitle}
                numberOfLines={2} >Shiro</Text>
            </TouchableOpacity>

            <TouchableOpacity style={_sty.mx5}>
              <ImageBackground
                style={_sty.featuredItemImage}
                source={require('../../tmp/images/res3.jpg')}
              />
              <Text
                style={_sty.featuredItemTitle}
                numberOfLines={2} >Chicken Republic - Awolowo 2</Text>
            </TouchableOpacity>

            <TouchableOpacity style={_sty.mx5}>
              <ImageBackground
                style={_sty.featuredItemImage}
                source={require('../../tmp/images/res4.jpg')}
              />
              <Text
                style={_sty.featuredItemTitle}
                numberOfLines={2} >The Place - Ikoyi</Text>
            </TouchableOpacity>

            <TouchableOpacity style={_sty.mx5}>
              <ImageBackground
                style={_sty.featuredItemImage}
                source={require('../../tmp/images/res5.jpg')}
              />
              <Text
                style={_sty.featuredItemTitle}
                numberOfLines={2} >KFC - Adeola Odeku</Text>
            </TouchableOpacity>

            <TouchableOpacity style={_sty.mx5}>
              <ImageBackground
                style={_sty.featuredItemImage}
                source={require('../../tmp/images/res6.jpg')}
              />
              <Text
                style={_sty.featuredItemTitle}
                numberOfLines={2} >Pizza Hut - Ikoyi</Text>
            </TouchableOpacity>

          </ScrollView>
        </View>
        {/* /Featured view */}

        <TouchableOpacity
          style={_sty.itemContainer}
          onPress={() => navigation.navigate('RestaurantDetails')}
        >
          <ImageBackground
            style={_sty.ItemImage}
            source={require('../../tmp/images/res7.png')}>
            <View style={_sty.ItemImageContentBottom}>
              <Text style={_sty.itemDeliveryTime}>20 - 25 MIN</Text>
            </View>
          </ImageBackground>
          <View style={_sty.cpx}>
            <Text style={[_sty.h1, _sty.mb5]}>KFC - Adeola Odeku</Text>
            <Text style={_sty.itemCusines} numberOfLines={2}>African, Breakfast, Sandwiches, Salads, Vegetarian</Text>
            <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
          </View>
        </TouchableOpacity>


        <TouchableOpacity style={_sty.itemContainer}>
          <ImageBackground
            style={_sty.ItemImage}
            source={require('../../tmp/images/res6.jpg')}>
            <View style={_sty.ItemImageContentBottom}>
              <Text style={_sty.itemDeliveryTime}>30 - 35 MIN</Text>
            </View>
          </ImageBackground>
          <View style={_sty.ItemContents}>
            <Text style={[_sty.h1, _sty.mb5]}>Micos Chicken and Waffles</Text>
            <Text style={_sty.itemCusines} numberOfLines={2}>African, Breakfast, Sandwiches, Salads, Vegetarian</Text>
            <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
          </View>
        </TouchableOpacity>


        <TouchableOpacity style={_sty.itemContainer}>
          <ImageBackground
            style={_sty.ItemImage}
            source={require('../../tmp/images/res3.jpg')}>
            <View style={_sty.ItemImageContentBottom}>
              <Text style={_sty.itemDeliveryTime}>20 - 25 MIN</Text>
            </View>
          </ImageBackground>
          <View style={_sty.ItemContents}>
            <Text style={[_sty.h1, _sty.mb5]}>Majik Juice Lekki</Text>
            <Text style={_sty.itemCusines} numberOfLines={2}>African, Breakfast, Sandwiches, Salads, Vegetarian</Text>
            <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
          </View>
        </TouchableOpacity>


        <TouchableOpacity style={_sty.itemContainer}>
          <ImageBackground
            style={_sty.ItemImage}
            source={require('../../tmp/images/res2.jpg')}>
            <View style={_sty.ItemImageContentBottom}>
              <Text style={_sty.itemDeliveryTime}>20 - 25 MIN</Text>
            </View>
          </ImageBackground>
          <View style={_sty.ItemContents}>
            <Text style={[_sty.h1, _sty.mb5]}>Mint By Eat Green Company</Text>
            <Text style={_sty.itemCusines} numberOfLines={2}>African, Breakfast, Sandwiches, Salads, Vegetarian</Text>
            <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
          </View>
        </TouchableOpacity>


        <TouchableOpacity style={_sty.itemContainer}>
          <ImageBackground
            style={_sty.ItemImage}
            source={require('../../tmp/images/res1.png')}>
            <View style={_sty.ItemImageContentBottom}>
              <Text style={_sty.itemDeliveryTime}>20 - 25 MIN</Text>
            </View>
          </ImageBackground>
          <View style={_sty.ItemContents}>
            <Text style={[_sty.h1, _sty.mb5]}>Mint By Eat Green Company</Text>
            <Text style={_sty.itemCusines} numberOfLines={2}>African, Breakfast, Sandwiches, Salads, Vegetarian</Text>
            <Text style={_sty.itemData} numberOfLines={1}>Min Order: 500 / Delivery Fee: 600</Text>
          </View>
        </TouchableOpacity>





      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
